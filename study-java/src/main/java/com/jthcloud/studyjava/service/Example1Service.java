package com.jthcloud.studyjava.service;

import com.jthcloud.studyjava.model.Example1Item;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
public class Example1Service {
    /**
     * 샘플 데이터를 생성한다.
     *
     * @return Example1Item 리스트
     */
    private List<Example1Item> settingData(){
        List<Example1Item> result = new LinkedList<>(); // Example1Item의 비어진 리스트

        Example1Item item1 = new Example1Item(); // Example1Item 리스트에 Example1Item item1을 생성하였다.
        item1.setName("홍길동"); // item1의 이름을 홍길동이라고 넣었다.
        item1.setBirthday(LocalDate.of(1234,5,6)); // item1의 생년월일을 1234년 5월 6일로 넣었다.
        result.add(item1); // 이름과 생년월일을 넣은 아이템 1이 Exampl1Item에 저장된다.

        Example1Item item2 = new Example1Item(); // Example1Item 리스트에 Example1Item item2을 생성하였다.
        item2.setName("정태화"); // item2의 이름을 정태화라고 넣었다.
        item2.setBirthday(LocalDate.of(1998,7,29)); // item2의 생년월일을 1998년 7월 29일로 넣었다.
        result.add(item2); // 이름과 생년월일을 넣은 아이템 2이 Exampl1Item에 저장된다.

        Example1Item item3 = new Example1Item(); // EExample1Item 리스트에 Example1Item item3을 생성하였다.
        item3.setName("박광일"); // item3의 이름을 박광일이라고 넣었다.
        item3.setBirthday(LocalDate.of(1998,7,28)); // item3의 생년월일을 1998년 7월 28일로 넣었다.
        result.add(item3); // 이름과 생년월일을 넣은 아이템 3이 Exampl1Item에 저장된다.

        Example1Item item4 = new Example1Item(); // Example1Item 리스트에 Example1Item item4을 생성하였다.
        item4.setName("박근홍"); // item4의 이름을 박근홍이라고 넣었다.
        item4.setBirthday(LocalDate.of(1998,6,10)); // item4의 생년월일을 1998년 6월 10일로 넣었다.
        result.add(item4); // 이름과 생년월일을 넣은 아이템 4이 Exampl1Item에 저장된다.

        Example1Item item5 = new Example1Item(); // Example1Item 리스트에 Example1Item item5을 생성하였다.
        item5.setName("한상진"); // item5의 이름을 한상진이라고 넣었다.
        item5.setBirthday(LocalDate.of(1998,8,13)); // item5의 생년월일을 1998년 8월 13일로 넣었다.
        result.add(item5); // 이름과 생년월일을 넣은 아이템 5이 Exampl1Item에 저장된다.

        return result; // 반복한다.
    }

    public List<String>getForEachTest(){
        List<Example1Item> data = settingData(); // Example1Item 리스트 데이터에 세팅데이터를 갖고 와 Example1Item 리스트를 받아서 넣는다.
        List<String> result = new LinkedList<>(); // 문자열의 리스트 변수 생성

        data.forEach(item -> result.add(item.getName()) ); // 데이터에 있는 각 item들의 이름을 문자열 리스트 변수에 하나씩 들어간다.

        for (Example1Item item : data){ // Example1Item 리스트 data에 item이 없을 때까지 반복한다.
            result.add(item.getName()); // item의 이름을 문자열 리스트 변수에 넣는다.
        }

        return result; // 문자열 리스트 변수를 반복한다.
    }


}
